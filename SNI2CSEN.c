//************************************************
//  I2C Sensors module
//  Tumanov Stanislav Alexandrovich 
//  18/10/2021
// Интервал не менее 20мс
//************************************************


#define BME280ADDR0 0x76      //The I2C Address of the BME280
#define BME280ADDR1 0x77      //The I2C Address of the BME280
#define LM75ADDR0 0x48      //The I2C Address of the LM75A 
#define LM75ADDR1 0x49      //The I2C Address of the LM75A 
#define LM75ADDR2 0x4A      //The I2C Address of the LM75A 
#define BMP180ADDR 0x77      //The I2C Address of the BMP180 
#define AM2320ADDR 0xB8      //The I2C Address of the AM2320
#define BH1750ADDR 0x23     //The I2C Address of the BH1750
#define PM2005ADDR 0x50     //The I2C Address of the PM2005
#define SHT2XADDR 0x40     //The I2C Address of the SHT2X
#define SHT3XADDR0 0x44     //The I2C Address of the SHT3X
#define SHT3XADDR1 0x45     //The I2C Address of the SHT3X

#ifndef noBME280
//----------------- BME280-0 ---------------
int1           BME280_0_device_valid = 0;
#define        SNI2CSENBME280_0_humidity 0
unsigned int8  BME280_0_humidity = 0;
unsigned int8  BME280_0_humidity_last = 0;
#define        SNI2CSENBME280_0_pressure 1
unsigned int32 BME280_0_pressure = 0;
unsigned int32 BME280_0_pressure_last = 0;
#define        SNI2CSENBME280_0_temperature 2
float          BME280_0_temperature = 0;
float          BME280_0_temperature_last = 0;
//----------------- BME280-1 ---------------
int1           BME280_1_device_valid = 0;
#define        SNI2CSENBME280_1_humidity 3
unsigned int8  BME280_1_humidity = 0;
unsigned int8  BME280_1_humidity_last = 0;
#define        SNI2CSENBME280_1_pressure 4
unsigned int32 BME280_1_pressure = 0;
unsigned int32 BME280_1_pressure_last = 0;
#define        SNI2CSENBME280_1_temperature 5
float          BME280_1_temperature = 0;
float          BME280_1_temperature_last = 0;

void BME280_get_data();

void BME280init();
void BME280init0();
void BME280init1();
void BME280init2();
void BME280init3();
void BME280init4();
void BME280init5();
void BME280init6();
void BME280init7();
void BME280setAddres(unsigned int8 addr);

float BME280_get_Temp();
float BME280_get_Hum();
float BME280_get_Pess();
unsigned int8 BME280_getID();


unsigned int8 BME280_addr = BME280ADDR0;
signed int32 BME280_t_fine;

int32 BME280_rawpres;
int32 BME280_rawtemp;
int32 BME280_rawhum;
#endif

int16 BME280_Dig_T1;
signed   int16 BME280_Dig_T2_BMP180_ac1;
signed   int16 BME280_Dig_T3_BMP180_ac2;

unsigned int16 BME280_Dig_H1_BMP180_ac4;
signed   int16 BME280_Dig_H2_BMP180_ac3;
unsigned int16 BME280_Dig_H3_BMP180_ac5;
signed   int16 BME280_Dig_H4_BMP180_b1;
signed   int16 BME280_Dig_H5_BMP180_b2;
signed   int16 BME280_Dig_H6_BMP180_mb;

unsigned int16 BME280_Dig_P1_BMP180_ac6;
signed   int16 BME280_Dig_P2_BMP180_mc;
signed   int16 BME280_Dig_P3_BMP180_md;
signed   int16 BME280_Dig_P4;
signed   int16 BME280_Dig_P5;
signed   int16 BME280_Dig_P6;
signed   int16 BME280_Dig_P7;
signed   int16 BME280_Dig_P8;
signed   int16 BME280_Dig_P9;
unsigned int8 BME280_ID = 0;

//----------------- LM75A ---------------

//----------------- LM75A-0 ---------------
int1           LM75A_0_device_valid = 0;
#define        SNI2CSENLM75A_0_temperature 6
float          LM75A_0_temperature = 0;
float          LM75A_0_temperature_last = 0;

//----------------- LM75A-1 ---------------
int1           LM75A_1_device_valid = 0;
#define        SNI2CSENLM75A_1_temperature 7
float          LM75A_1_temperature = 0;
float          LM75A_1_temperature_last = 0;

//----------------- LM75A-2 ---------------
int1           LM75A_2_device_valid = 0;
#define        SNI2CSENLM75A_2_temperature 8
float          LM75A_2_temperature = 0;
float          LM75A_2_temperature_last = 0;

void LM75A_read_temp(unsigned int8 addr);
float LM75A_get_Temp(unsigned int8 addr);

//----------------- BMP180 ---------------
#define BMP180_P_CORRECTION   1.5

int1           BMP180_device_valid = 0;
#define        SNI2CSENBMP180_temperature 9
float          BMP180_temperature = 0;
float          BMP180_temperature_last = 0;
#define        SNI2CSENBMP180_pressure 10
unsigned int32 BMP180_pressure = 0;
unsigned int32 BMP180_pressure_last = 0;
// floating point cal factors
float BMP180__c3;
float BMP180__c4;
float BMP180__b1;
float BMP180__c5;
float BMP180__c6;
float BMP180__mc;
float BMP180__md;

// polynomomial constants
float BMP180__x0;
float BMP180__x1;
float BMP180__x2;
float BMP180__y0;
float BMP180__y1;
float BMP180__y2;
float BMP180__p0;
float BMP180__p1;
float BMP180__p2;

float BMP180__s;     // T-25, used in pressure calculation - must run temperature reading before pressure reading
unsigned int32 BMP180_PressureRAW = 0;
float BMP180_tempRAW = 0;

void BMP180_calibration0();
void BMP180_calibration1();
void BMP180_calibration2();
void BMP180_calibration3();
void BMP180_readTempBegin();
void BMP180_readTempEnd();
void BMP180_readPressureRAWBegin();
void BMP180_readPressureRAWEnd();
float BMP180_get_Temp();
unsigned int32 BMP180_get_Pressure();
int1 BMP180_isBMP180();

//----------------- AM2320 ---------------
int1           AM2320_device_valid = 0;
#define        SNI2CSENAM2320_temperature 11
float          AM2320_temperature = 0;
float          AM2320_temperature_last = 0;

#define        SNI2CSENAM2320_humidity 12
unsigned int8  AM2320_humidity = 0;
unsigned int8  AM2320_humidity_last = 0;

unsigned char AM2320_buffer[8]; 

void AM2320_request();
void AM2320_read();
int1 AM2320_crc();
float AM2320_get_Temp();
unsigned int16 AM2320_get_Hum();


//----------------- BH1750 ---------------
int1           BH1750_device_valid = 0;
#define        SNI2CSENBH1750_light 13

unsigned int16  BH1750_lightLevel = 0;
unsigned int16  BH1750_lightLevel_last = 0;

unsigned int16 BH1750_getlightLevel();

//----------------- PM2005 ---------------
int1           PM2005_device_valid = 0;
#define        SNI2CSENPM2005_PM25 14
#define        SNI2CSENPM2005_PM10 15

unsigned int16  PM2005_PM25Level = 0;
unsigned int16  PM2005_PM25Level_last = 0;

unsigned int16  PM2005_PM10Level = 0;
unsigned int16  PM2005_PM10Level_last = 0;

void PM2005_init();
void PM2005_read();

//----------------- SHT2X ---------------
//Протестированно 12.10.2021 на STH20
int1           SHT2X_device_valid = 0;
#define        SNI2CSENSHT2X_temperature 16
float          SHT2X_temperature = 0;
float          SHT2X_temperature_last = 0;

#define        SNI2CSENSHT2X_humidity 17
unsigned int8  SHT2X_humidity = 0;
unsigned int8  SHT2X_humidity_last = 0;

int1 SHT2X_init();
void SHT2X_readTemperatureBegin();
void SHT2X_readTemperatureEnd();
float SHT2X_getTemperature();
void SHT2X_readHumidityBegin();
void SHT2X_readHumidityEnd();
unsigned int8 SHT2X_getHumidity();

//----------------- SHT3X ---------------
//Протестированно 12.10.2021 на STH35
int1           SHT3X_0_device_valid = 0;
#define        SNI2CSENSHT3X_0_temperature 18
float          SHT3X_0_temperature = 0;
float          SHT3X_0_temperature_last = 0;

#define        SNI2CSENSHT3X_0_humidity 19
unsigned int8  SHT3X_0_humidity = 0;
unsigned int8  SHT3X_0_humidity_last = 0;

int1           SHT3X_1_device_valid = 0;
#define        SNI2CSENSHT3X_1_temperature 20
float          SHT3X_1_temperature = 0;
float          SHT3X_1_temperature_last = 0;

#define        SNI2CSENSHT3X_1_humidity 21
unsigned int8  SHT3X_1_humidity = 0;
unsigned int8  SHT3X_1_humidity_last = 0;

unsigned int16 SHT3X_temp;
unsigned int16 SHT3X_humidity;

void SHT3X_init(unsigned int8 addr);
void SHT3X_readBegin(unsigned int8 addr);
void SHT3X_readEnd(unsigned int8 addr);

float SHT3X_getTemperature();
unsigned int8 SHT3X_getHumidity();

//----------------- SNI2CSEN --------------
#define        SNI2CSENnoSensors 250
#define        SNI2CSENdebug 251

typedef void(*SNI2CSENPOn)(unsigned int8);
unsigned int8 SNI2CSEN_state = 0;
unsigned int8 SNI2CSEN_sensorsCount = 0;
unsigned int8 SNI2CSEN_numberOfSensors = 0;
unsigned int16 SNI2CSEN_debug = 0;
unsigned int8 SNI2CSEN_amountCrcDifferences;

unsigned int8  SNI2CSEN_highRegister = 0;
unsigned int8  SNI2CSEN_lowRegister = 0;

void SNI2CSENDefaultAction(unsigned int8 channel) {}
SNI2CSENPOn SNI2CSENAction = SNI2CSENDefaultAction;

int1 SNI2CSENcheckDev(unsigned int8 addr);
unsigned int8 SNI2CSENcrc8(unsigned int8 msb, unsigned int8 lsb);
unsigned int8 SNI2CSENreadByte(unsigned int8 devaddr, unsigned int8 address);
unsigned int16 SNI2CSENreadInt(unsigned int8 devaddr, unsigned int8 address);
unsigned int16 SNI2CSENreadIntInv(unsigned int8 devaddr, unsigned int8 address);
void SNI2CSENwriteByte(unsigned int8 devaddr, unsigned int8 address, unsigned int8 data);
float SNI2CSENgetRAWtoTemp(unsigned int16 vol);
void SNI2CSENcheck();
//==================================================================================================================
//================== BME280 ================
#ifndef noBME280
void BME280init()
{   
   BME280init0();
   delay_ms(1000);
   BME280init1();
   BME280init2();
   BME280init3();
   BME280init4();
   BME280init5();
   BME280init6();
   BME280init7();
}

void BME280init0() {   
   SNI2CSENwriteByte(BME280_addr,0xF4,0x00);   //Put it in sleep mode
}

void BME280init1() { //1.6ms  
   SNI2CSENwriteByte(BME280_addr,0xF5,0x84);   //Set Config Word
   SNI2CSENwriteByte(BME280_addr,0xF2,0x04);   //Set Humidity Control
   SNI2CSENwriteByte(BME280_addr,0xF4,0x93);   //Set Temp & Press Control + Mode
}

void BME280init2() {  //1.74ms
   BME280_Dig_T1 = SNI2CSENreadIntInv(BME280_addr,0x88);
   BME280_Dig_T2_BMP180_ac1 = SNI2CSENreadIntInv(BME280_addr,0x8A);
   BME280_Dig_T3_BMP180_ac2 = SNI2CSENreadIntInv(BME280_addr,0x8C);
}

void BME280init3() { //1.54ms
   BME280_Dig_H1_BMP180_ac4 = SNI2CSENreadByte(BME280_addr,0xA1);
   BME280_Dig_H2_BMP180_ac3 = SNI2CSENreadIntInv(BME280_addr,0xE1);
   BME280_Dig_H3_BMP180_ac5 = SNI2CSENreadByte(BME280_addr,0xE3);
}

void BME280init4() {  //2.32ms
   BME280_Dig_H4_BMP180_b1 = (((int16)SNI2CSENreadByte(BME280_addr,0xE4)<<4)|(SNI2CSENreadByte(BME280_addr,0xE5)& 0x0F)); //SO FAR BEST
   BME280_Dig_H5_BMP180_b2 = make16((SNI2CSENreadByte(BME280_addr,0xE5)>>4), SNI2CSENreadByte(BME280_addr,0xE6));
   BME280_Dig_H6_BMP180_mb = SNI2CSENreadByte(BME280_addr,0xE7);
}

void BME280init5() {
   BME280_Dig_P1_BMP180_ac6 = SNI2CSENreadIntInv(BME280_addr,0x8E);
   BME280_Dig_P2_BMP180_mc = SNI2CSENreadIntInv(BME280_addr,0x90);
   BME280_Dig_P3_BMP180_md = SNI2CSENreadIntInv(BME280_addr,0x92);
}

void BME280init6() {   
   BME280_Dig_P4 = SNI2CSENreadIntInv(BME280_addr,0x94);
   BME280_Dig_P5 = SNI2CSENreadIntInv(BME280_addr,0x96);
   BME280_Dig_P6 = SNI2CSENreadIntInv(BME280_addr,0x98);
}

void BME280init7() {   
   BME280_Dig_P7 = SNI2CSENreadIntInv(BME280_addr,0x9A);
   BME280_Dig_P8 = SNI2CSENreadIntInv(BME280_addr,0x9C);
   BME280_Dig_P9 = SNI2CSENreadIntInv(BME280_addr,0x9E);
}

void BME280setAddres(unsigned int8 addr) {
   BME280_addr = addr;
}

float BME280_get_Pess() { // 3.22ms
   float var1, var2, p; 
   var1 = ((float)BME280_t_fine/2.0) - 64000.0;
   var2 = var1 * var1 * ((float)BME280_Dig_P6) / 32768.0;
   var2 = var2 + var1 * ((float)BME280_Dig_P5) * 2.0;  
   var2 = (var2/4.0)+(((float)BME280_Dig_P4) * 65536.0);
   var1 = (((float)BME280_Dig_P3_BMP180_md) * var1 * var1 / 524288.0 + ((float)BME280_Dig_P2_BMP180_mc) * var1) / 524288.0;
   var1 = (1.0 + var1 / 32768.0)*((float)BME280_Dig_P1_BMP180_ac6);
   if (var1 == 0.0)  return 0; // avoid exception caused by division by zero
   p = 1048576.0 - (float)BME280_rawpres;
   p = (p - (var2 / 4096.0)) * 6250.0 / var1;
   var1 = ((float)BME280_Dig_P9) * p * p / 2147483648.0;
   var2 = p * ((float)BME280_Dig_P8) / 32768.0;
   p = p + (var1 + var2 + ((float)BME280_Dig_P7)) / 16.0;

   return(p);
}

float BME280_get_Hum() {
   signed int32 var1;
   var1 = (BME280_t_fine - ((int32)76800));
   var1 = (((((BME280_rawhum << 14) - (((int32)BME280_Dig_H4_BMP180_b1) << 20) - (((int32)BME280_Dig_H5_BMP180_b2) * var1)) + ((int32)16384)) >> 15) * (((((((var1 * ((int32)BME280_Dig_H6_BMP180_mb)) >> 10) * (((var1 * ((int32)BME280_Dig_H3_BMP180_ac5)) >> 11) + ((int32)32768))) >> 10) + ((int32)2097152)) *((int32)BME280_Dig_H2_BMP180_ac3) + 8192) >> 14));
   var1 = (var1 - (((((var1 >> 15) * (var1 >> 15)) >> 7) * ((int32)BME280_Dig_H1_BMP180_ac4)) >> 4));
   var1 = (var1 < 0 ? 0 : var1);
   var1 = (var1 > 419430400 ? 419430400 : var1);
   var1= var1 >> 12;
   return((float)var1/1024.0);
}

float BME280_get_Temp() {
   float var1, var2, T;
   var1 = (((float)BME280_rawtemp)/16384.0 - ((float)BME280_Dig_T1)/1024.0) * ((float)BME280_Dig_T2_BMP180_ac1);
   var2 = ((((float)BME280_rawtemp)/131072.0 - ((float)BME280_Dig_T1)/8192.0) *
   (((float)BME280_rawtemp)/131072.0 - ((float) BME280_Dig_T1)/8192.0)) * ((float)BME280_Dig_T3_BMP180_ac2);
   BME280_t_fine = (signed int32)(var1 + var2);
   return ((var1 + var2) / 5120.0);
}

unsigned int8 BME280_getID() {
   return SNI2CSENreadByte(BME280_addr,0xD0);
}

void BME280_get_data() { // 1.22ms
   int tempaddress = 0;
   int Temp[8];

   tempaddress = BME280_addr <<1;    //Left Shift 1 bit to allow space for R/W bit
//   tempaddress |= writebit;         //OR the shifted address with the WRITE Bit
   i2c_start();                  //Start I2C sequence
   i2c_write(tempaddress);          //Address the device with shifted address with WRITE bit set
   i2c_write(0xF7);         //Tell the Device Which Register you want to read
   
//   tempaddress = BME280 <<1;    //Left Shift 1 bit to allow space for R/W bit
   tempaddress |= 0x01;            //OR the shifted address with the READ Bit
   i2c_start();                  //Start I2C sequence
   i2c_write(tempaddress);          //Address the device with shifted address with READ bit set
   Temp[0]= i2c_read(1);            //0xF7
   Temp[1]= i2c_read(1);            //0xF8
   Temp[2]= i2c_read(1);            //0xF9
   Temp[3]= i2c_read(1);            //0xFa
   Temp[4]= i2c_read(1);            //0xFb
   Temp[5]= i2c_read(1);            //0xFc
   Temp[6]= i2c_read(1);            //0xFd
   Temp[7]= i2c_read(0);            //0xFe
   i2c_stop();                     //Stop The I2C transaction
   
   BME280_rawpres = make32(0x00, Temp[0], Temp[1],Temp[2]);
   BME280_rawpres >>= 4;

   BME280_rawtemp = make32(0x00, Temp[3], Temp[4],Temp[5]);
   BME280_rawtemp >>= 4;

   BME280_rawhum = make32(0x00,0x00,Temp[6],Temp[7]);
}
#endif

// ============== LM75A ==================
void LM75A_read_temp(unsigned int8 addr) {
   BME280_Dig_H1_BMP180_ac4 = SNI2CSENreadInt(addr, 0x00);
}

float LM75A_get_Temp() {
   return SNI2CSENgetRAWtoTemp(BME280_Dig_H1_BMP180_ac4);
}

// ============== BMP180 ==================
void BMP180_calibration0() { //3.4ms
   // read BMP180 EEPROM cal factors
   BME280_Dig_T2_BMP180_ac1 = SNI2CSENreadInt(BMP180ADDR, 0xAA);
   BME280_Dig_T3_BMP180_ac2 = SNI2CSENreadInt(BMP180ADDR, 0xAC);
   BME280_Dig_H2_BMP180_ac3 = SNI2CSENreadInt(BMP180ADDR, 0xAE);
   BME280_Dig_H1_BMP180_ac4 = SNI2CSENreadInt(BMP180ADDR, 0xB0);
   BME280_Dig_H3_BMP180_ac5 = SNI2CSENreadInt(BMP180ADDR, 0xB2);
   BME280_Dig_P1_BMP180_ac6 = SNI2CSENreadInt(BMP180ADDR, 0xB4);
}

void BMP180_calibration1() {
   BME280_Dig_H4_BMP180_b1  = SNI2CSENreadInt(BMP180ADDR, 0xB6);
   BME280_Dig_H5_BMP180_b2  = SNI2CSENreadInt(BMP180ADDR, 0xB8);
   BME280_Dig_H6_BMP180_mb  = SNI2CSENreadInt(BMP180ADDR, 0xBA);
   BME280_Dig_P2_BMP180_mc  = SNI2CSENreadInt(BMP180ADDR, 0xBC);
   BME280_Dig_P3_BMP180_md  = SNI2CSENreadInt(BMP180ADDR, 0xBE);
}

void BMP180_calibration2() { // 0.44ms
   //output_high(PIN_B7);
    // calculate floating point cal factors
   BMP180__c3 = 0.0048828125 * BME280_Dig_H2_BMP180_ac3;            // 160 * pow2(-15) * BMP180_ac3;
   BMP180__c4 = 0.000000030517578125 * BME280_Dig_H1_BMP180_ac4;    // 1E-3 * pow2(-15) * ac4;
   BMP180__c5 = 0.00000019073486328125 * BME280_Dig_H3_BMP180_ac5;  // (pow2(-15)/160) * ac5;
   BMP180__c6 = (float)BME280_Dig_P1_BMP180_ac6;
   BMP180__b1 = 0.00002384185791015625 * BME280_Dig_H4_BMP180_b1;   // 25600 * pow2(-30) * BMP180_b1;
   BMP180__mc = 0.08 * BME280_Dig_P2_BMP180_mc;                     // (pow2(11) / 25600) * mc;
   BMP180__md = (float)BME280_Dig_P3_BMP180_md / 160;
   //output_low(PIN_B7);
}

void BMP180_calibration3() {
   // calculate polynomial constants
   BMP180__x0 = (float)BME280_Dig_T2_BMP180_ac1;
   BMP180__x1 = 0.01953125 * BME280_Dig_T3_BMP180_ac2;             // 160 * pow2(-13) * BMP180_ac2;
   BMP180__x2 = 0.000762939453125 * BME280_Dig_H5_BMP180_b2;       // 25600 * pow2(-25) * BMP180_b2;
   BMP180__y0 = BMP180__c4 * 32768;                  //BMP180__c4 * pow2(15);
   BMP180__y1 = BMP180__c4 * BMP180__c3;
   BMP180__y2 = BMP180__c4 * BMP180__b1;
   BMP180__p0 = 2.364375; 
   BMP180__p1 = 0.992984;
   BMP180__p2 = 0.000004421; 
}

void BMP180_readTempBegin() {
   SNI2CSENwriteByte(BMP180ADDR, 0xF4, 0x2E);
}

void BMP180_readTempEnd() {
   BMP180_tempRAW = SNI2CSENreadInt(BMP180ADDR, 0xF6);
}

void BMP180_readPressureRAWBegin() {
   const int8 OVS_S = 3; // Oversampling Setting (0,1,2,3 from ultra low power, to ultra hi-resolution)
  // Write 0x34+(OSS<<6) into register 0xF4
  // Request a pressure reading w/ oversampling setting
  SNI2CSENwriteByte(BMP180ADDR, 0xF4, (0x34 + (OVS_S<<6)) );
}

void BMP180_readPressureRAWEnd() {
   int8 msb, lsb, xlsb;
   float p;

  // Read register 0xF6 (MSB), 0xF7 (LSB), and 0xF8 (XLSB)
   msb  = SNI2CSENreadByte(BMP180ADDR, 0xF6);
   lsb  = SNI2CSENreadByte(BMP180ADDR, 0xF7);
   xlsb = SNI2CSENreadByte(BMP180ADDR, 0xF8);
   p = (256*msb) + lsb + (xlsb/256);
   BMP180_PressureRAW = p;
}

float BMP180_get_Temp() {
   float alpha, T;
   alpha = BMP180__c5 * (BMP180_tempRAW - BMP180__c6);
   T = alpha + (BMP180__mc/(alpha + BMP180__md));
   BMP180__s = T - 25;
   return(T);
}

unsigned int32 BMP180_get_Pressure() {
   float x, y, z;
   float P;
   x = BMP180__x2*BMP180__s*BMP180__s + BMP180__x1*BMP180__s + BMP180__x0;
   y = BMP180__y2*BMP180__s*BMP180__s + BMP180__y1*BMP180__s + BMP180__y0;
   z = ((float)(BMP180_PressureRAW) - x) / y;
   P = BMP180__p2*z*z + BMP180__p1*z + BMP180__p0;
   P += BMP180_P_CORRECTION;
   return(P*100);
}

int1 BMP180_isBMP180() {
      return (SNI2CSENreadByte(BMP180ADDR, 0xD0) == 0x55);
}

// ============== AM2320 ==================
void AM2320_request() {
   //Wake_Sensor
   i2c_start();
   i2c_write(AM2320ADDR);
   i2c_stop(); 
   //request sensor_temperature and humidity
   i2c_start();
   i2c_write(AM2320ADDR);
   i2c_write(0x03);
   i2c_write(0x00);
   i2c_write(0x04);
   i2c_stop();
}

void AM2320_read() {
   i2c_start(); 
   i2c_write(AM2320ADDR | 0x01); 
   for (unsigned int8 i = 0; i < 7; i ++) AM2320_buffer[i] = i2c_read(1);
   AM2320_buffer[7] = i2c_read(0);
   i2c_stop();
}

int1 AM2320_crc() {
   unsigned int16 crc = 0xFFFF; 
   unsigned int8 s = 0x00; 
   unsigned int8 len = 6;
   unsigned char *ptr = AM2320_buffer;

   while( len -- ) { 
      crc ^= *ptr ++;
      for (s = 0; s < 8; s ++)  { 
         if ((crc & 0x01) != 0)  { 
         crc >>= 1; 
         crc ^= 0xA001; 
         } 
         else { 
         crc >>= 1; 
         } 
      } 
   } 
   return (crc == make16(AM2320_buffer[7], AM2320_buffer[6]));
}

float AM2320_get_Temp() {
   BME280_Dig_H1_BMP180_ac4 =  AM2320_buffer[4] & 0x7F; 
   BME280_Dig_H1_BMP180_ac4 <<= 8; 
   BMP180_tempRAW = BME280_Dig_H1_BMP180_ac4 + AM2320_buffer[5]; 
   BMP180_tempRAW /= 10;								//	Температура = data[4]data[5], где старший бит указывает на знак (0-плюс, 1-минус).
   if(AM2320_buffer[4] & 0x80) BMP180_tempRAW *= -1; 
   return BMP180_tempRAW;
}

void AM2320_wake() {
   //Wake_Sensor
   i2c_start();
   i2c_write(AM2320ADDR);
   i2c_stop(); 
}

unsigned int16 AM2320_get_Hum() {
   return (make16(AM2320_buffer[2], AM2320_buffer[3]) / 10);
}

//----------------- BH1750 ---------------

unsigned int16 BH1750_getlightLevel() {
   unsigned int16 ret = SNI2CSENreadInt(BH1750ADDR, 0x11);
   return ret / 1.2;
}

//----------------- PM2005 ---------------
// int1           PM2005_device_valid = 0;
// #define        SNI2CSENPM2005_PM25 14
// #define        SNI2CSENPM2005_PM10 15

// unsigned int16  PM2005_PM25Level = 0;
// unsigned int16  PM2005_PM25Level_last = 0;

// unsigned int16  PM2005_PM10Level = 0;
// unsigned int16  PM2005_PM10Level_last = 0;

void PM2005_init() {
   i2c_start();
   i2c_write(PM2005ADDR);
   i2c_write(0x16);
   i2c_write(0x07);
   i2c_write(0x03);
   i2c_write(0x00);
   i2c_write(0x24);
   i2c_write(0x00);
   i2c_write(0x36);
   i2c_stop(); 
}

void PM2005_read() {
   int8 state = 0;
   int16 pm25 = 0;
   int16 pm10 = 0;
   int8 mode = 0;

   i2c_start();
   i2c_write(PM2005ADDR | 0x01);
   i2c_read();
   i2c_read();
   state = i2c_read();
   i2c_read();
   i2c_read();

   PM2005_PM25Level_last = PM2005_PM25Level;
   PM2005_PM25Level = i2c_read();
   PM2005_PM25Level = pm25 << 8;
   PM2005_PM25Level += i2c_read();

   PM2005_PM10Level_last = PM2005_PM10Level;
   PM2005_PM10Level = i2c_read();
   PM2005_PM10Level = pm10 << 8;
   PM2005_PM10Level += i2c_read();  

   mode = i2c_read();
   i2c_read();
   i2c_read();
   i2c_read();
   i2c_read();
   i2c_read();
   i2c_read();
   i2c_read();
   i2c_read();
   i2c_read();
   i2c_read();
   i2c_read(0); 
   i2c_stop(); 
}

//----------------- SHT2X ---------------

int1 SHT2X_init() {
   int1 r;
   i2c_start();
   i2c_write(SHT2XADDR << 1);
   r = i2c_write(0b11111110); //SHT20_RESET 0xFE
   i2c_stop();
   return !r;
}

unsigned int8 SHT2x_checkCrc(unsigned int8 msb, unsigned int8 lsb) {
  const unsigned int16 POLYNOMIAL1 = 0x131;
  unsigned int8 crc = 0;     
  unsigned int8 j;

  for (unsigned int8 i = 0; i < 2; ++ i)
  {  if (i) crc ^= lsb; else crc ^= msb;
    for (j = 8; j > 0; --j)
    { if (crc & 0x80) crc = (crc << 1) ^ POLYNOMIAL1;
      else crc = (crc << 1);
    }
  }
   return crc;
}

void SHT2X_readTemperatureBegin() {
   i2c_start();
   i2c_write(SHT2XADDR << 1);
   i2c_write(0b11110011);
   i2c_stop();
}

void SHT2X_readTemperatureEnd() {
   //unsigned int8 msb, lsb;
   
   i2c_start();
   i2c_write((SHT2XADDR << 1) | 0x01);
   SNI2CSEN_highRegister = i2c_read();
   SNI2CSEN_lowRegister = i2c_read();
   SNI2CSEN_amountCrcDifferences = i2c_read(0) - SHT2x_checkCrc(SNI2CSEN_highRegister, SNI2CSEN_lowRegister);  
   i2c_stop();
}

float SHT2X_getTemperature() {
   float temp_t;
   SNI2CSEN_lowRegister = SNI2CSEN_lowRegister & 0b11111100;
   temp_t = make16(SNI2CSEN_highRegister, SNI2CSEN_lowRegister);
   return (-46.85 + (175.72*(temp_t/65536)));
}

void SHT2X_readHumidityBegin() {
   i2c_start();
   i2c_write(SHT2XADDR << 1);
   i2c_write(0b11110101);
   i2c_stop();
}

void SHT2X_readHumidityEnd() {
   int16 humidum;
   i2c_start();
   i2c_write((SHT2XADDR << 1) | 0x01);
   SNI2CSEN_highRegister = i2c_read();
   SNI2CSEN_lowRegister = i2c_read();
   SNI2CSEN_amountCrcDifferences = i2c_read(0) - SHT2x_checkCrc(SNI2CSEN_highRegister, SNI2CSEN_lowRegister); 
   i2c_stop();
   // humidum = make16(SNI2CSEN_highRegister, SNI2CSEN_lowRegister & 0b11111100);
   // return (-6 + (0.00190734*humidum));
}

unsigned int8 SHT2X_getHumidity() {
   int16 humidum;
   humidum = make16(SNI2CSEN_highRegister, SNI2CSEN_lowRegister & 0b11111100);
   return (-6 + (0.00190734*humidum));
}

//----------------- SHT3X ---------------
#ifndef noSHT3X

void SHT3X_init(unsigned int8 addr) {
   i2c_start();
   i2c_write(addr << 1);
   i2c_write(0x30); //SOFTRESET 0x302A
   i2c_write(0xA2); 
   i2c_stop();
}

void SHT3X_readBegin(unsigned int8 addr) {
   i2c_start();
   i2c_write(addr << 1);
   i2c_write(0x27); 
   i2c_write(0x37);   
   i2c_stop();
   SNI2CSEN_amountCrcDifferences = 0;
}

void SHT3X_readEnd(unsigned int8 addr) {
   int8 msb, lsb, checksum;
   i2c_start();
   i2c_write(addr << 1);
   i2c_write(0xE0);
   i2c_write(0x00);
   i2c_start();
   i2c_write((addr << 1) | 0x01);
   msb = i2c_read();
   lsb = i2c_read();
   SNI2CSEN_amountCrcDifferences = i2c_read() - SNI2CSENcrc8(msb, lsb);

   SHT3X_temp = make16(msb, lsb);
   msb = i2c_read();
   lsb = i2c_read();
   SNI2CSEN_amountCrcDifferences += i2c_read() - SNI2CSENcrc8(msb, lsb);
   SHT3X_humidity = make16(msb, lsb);
   i2c_read(0);
   i2c_stop();
}

float SHT3X_getTemperature() {
   return ((float)SHT3X_temp * 175) / 65535 - 45;
}

unsigned int8 SHT3X_getHumidity() {
   return ((unsigned int32)SHT3X_humidity * 100) / 65535;
}
#endif
// ============== SNI2CSESORS ==================

int1 SNI2CSENcheckDev(unsigned int8 addr) {
   int1 r;
   i2c_start();
   i2c_write(addr);
   r = i2c_write(0);
   i2c_stop();
   return !r;
}

unsigned int8 SNI2CSENcrc8(unsigned int8 msb, unsigned int8 lsb) {
    unsigned int8 crc = 0xff;
    unsigned int8 i, j;
    for (i = 0; i < 2; i++) {
       if (i == 0) crc ^= msb;
       else crc ^= lsb;
        for (j = 0; j < 8; j++) {
            if ((crc & 0x80) != 0)
                crc = (unsigned int8)((crc << 1) ^ 0x31);
            else
                crc <<= 1;
        }
    }
    return crc;
}

unsigned int8 SNI2CSENreadByte(unsigned int8 devaddr, unsigned int8 address) {
   unsigned int8 SNI2CSENaddr = devaddr << 1;
   int8 data;
   i2c_start();
   i2c_write(SNI2CSENaddr);
   i2c_write(address);
   i2c_start();
   i2c_write(SNI2CSENaddr | 0x01 );
   data=i2c_read(0);
   i2c_stop();
   return(data);
}

unsigned int16 SNI2CSENreadInt(unsigned int8 devaddr, unsigned int8 address) {
   unsigned int8 SNI2CSENaddr = devaddr << 1;
   int8 msb, lsb;
   int16 temp;
   i2c_start();
   i2c_write(SNI2CSENaddr);
   i2c_write(address);
   i2c_start();
   i2c_write(SNI2CSENaddr | 0x01 );
   msb = i2c_read();
   lsb = i2c_read(0);
   i2c_stop();
   temp = make16(msb, lsb);
   return (temp);
}

unsigned int16 SNI2CSENreadIntInv(unsigned int8 devaddr, unsigned int8 address) {
   unsigned int8 SNI2CSENaddr = devaddr << 1;
   int8 msb, lsb;
   int16 temp;
   i2c_start();
   i2c_write(SNI2CSENaddr);
   i2c_write(address);
   i2c_start();
   i2c_write(SNI2CSENaddr | 0x01 );
   lsb = i2c_read();
   msb = i2c_read(0);
   i2c_stop();
   temp = make16(msb, lsb);
   return (temp);
}

void SNI2CSENwriteByte(unsigned int8 devaddr, unsigned int8 address, unsigned int8 data) {
   unsigned int8 SNI2CSENaddr = devaddr << 1;
   i2c_start();
   i2c_write(SNI2CSENaddr);
   i2c_write(address);
   i2c_write(data);
   i2c_stop();
}

float SNI2CSENgetRAWtoTemp(unsigned int16 vol) {
   float temp;
   unsigned int16 vl = vol;
   if ((vl >> 15) == 0) {
      temp = (vl * 0.00390625);
   } else {
      vl = 0xFFFF^vl;
      vl ++;
      temp = (vl * -0.00390625);
   }
   return temp;
}

void SNI2CSENcheck() {
   switch (SNI2CSEN_state) {
      #ifndef noBME280
      case 0:// BME280
         SNI2CSEN_sensorsCount = 0;
         BME280_addr = BME280ADDR0;
         BME280_0_device_valid = SNI2CSENcheckDev(BME280_addr << 1);
         BME280_ID = BME280_getID();
         if (BME280_0_device_valid && BME280_ID >= 0x58 && BME280_ID <= 0x60) SNI2CSEN_state = 2;
         else {SNI2CSEN_state = 1; BME280_0_device_valid = false;};
         break; 
      case 1:
         BME280_addr = BME280ADDR1;
         BME280_1_device_valid = SNI2CSENcheckDev(BME280_addr << 1);
         BME280_ID = BME280_getID();
         if (BME280_1_device_valid && BME280_ID >= 0x58 && BME280_ID <= 0x60) SNI2CSEN_state ++;
         else {SNI2CSEN_state = 17; BME280_1_device_valid = false;};
         break;
      case 2:
         SNI2CSEN_sensorsCount ++;
         BME280init0();
         SNI2CSEN_state ++;
         break;
      case 3:
         BME280init1();
         SNI2CSEN_state ++;
         break;
      case 4:
         BME280init2();
         SNI2CSEN_state ++;
         break;
      case 5:
         BME280init3();
         SNI2CSEN_state ++;
         break;
      case 6:
         BME280init4();
         SNI2CSEN_state ++;
         break; 
      case 7:
         BME280init5();
         SNI2CSEN_state ++;
         break;          
      case 8:
         BME280init6();
         SNI2CSEN_state ++;
         break; 
      case 9:
         BME280init7();
         SNI2CSEN_state ++;
         break; 
      case 10:
         BME280_get_data();
         SNI2CSEN_state ++;
         if (BME280_addr == BME280ADDR1) SNI2CSEN_state = 14;
         break;
      case 11:  
         BME280_0_pressure_last = BME280_0_pressure;
         BME280_0_pressure = (int32)BME280_get_Pess();
         (*SNI2CSENAction)(SNI2CSENBME280_0_pressure); 
         SNI2CSEN_state ++;
         break; 
      case 12: 
         BME280_0_temperature_last = BME280_0_temperature; 
         BME280_0_temperature = BME280_get_Temp();
         (*SNI2CSENAction)(SNI2CSENBME280_0_temperature); 
         if (BME280_ID != 0x60) SNI2CSEN_state = 14;
         else SNI2CSEN_state ++;
         break;            
      case 13: 
         BME280_0_humidity_last = BME280_0_humidity; 
         BME280_0_humidity = (int8)BME280_get_Hum();
         (*SNI2CSENAction)(SNI2CSENBME280_0_humidity); 
         SNI2CSEN_state = 1;
         break;
      case 14:  
         BME280_1_pressure_last = BME280_1_pressure;
         BME280_1_pressure = (int32)BME280_get_Pess();
         (*SNI2CSENAction)(SNI2CSENBME280_1_pressure);
         SNI2CSEN_state ++;
         break; 
      case 15:  
         BME280_1_temperature_last = BME280_1_temperature;
         BME280_1_temperature = BME280_get_Temp();
         (*SNI2CSENAction)(SNI2CSENBME280_1_temperature);
         if (BME280_ID != 0x60) SNI2CSEN_state = 17;
         else SNI2CSEN_state ++;
         break;            
      case 16:  
         BME280_0_humidity_last = BME280_0_humidity; 
         BME280_1_humidity = (int8)BME280_get_Hum();
         (*SNI2CSENAction)(SNI2CSENBME280_1_humidity); 
         SNI2CSEN_state ++;
         break;
         #endif
      case 17:// LM75_0
         LM75A_0_device_valid = SNI2CSENcheckDev(LM75ADDR0 << 1);
         if (LM75A_0_device_valid) SNI2CSEN_state ++;
         else SNI2CSEN_state = 20;
         break;  
      case 18:
         SNI2CSEN_sensorsCount ++;
         LM75A_read_temp(LM75ADDR0);
         SNI2CSEN_state ++;
         break;
      case 19:
         LM75A_0_temperature_last = LM75A_0_temperature;
         LM75A_0_temperature = LM75A_get_Temp();
         (*SNI2CSENAction)(SNI2CSENLM75A_0_temperature);
         SNI2CSEN_state ++;
         break;       
      case 20:// LM75_1
         LM75A_1_device_valid = SNI2CSENcheckDev(LM75ADDR1 << 1);
         if (LM75A_1_device_valid) SNI2CSEN_state ++;
         else SNI2CSEN_state = 23;
         break;  
      case 21:
         SNI2CSEN_sensorsCount ++;
         LM75A_read_temp(LM75ADDR1);
         SNI2CSEN_state ++;
         break;
      case 22:
         LM75A_1_temperature_last = LM75A_1_temperature;
         LM75A_1_temperature = LM75A_get_Temp();
         (*SNI2CSENAction)(SNI2CSENLM75A_1_temperature);
         SNI2CSEN_state ++;
         break;       
      case 23:// LM75_2
         LM75A_2_device_valid = SNI2CSENcheckDev(LM75ADDR2 << 1);
         if (LM75A_2_device_valid) SNI2CSEN_state ++;
         else SNI2CSEN_state = 26;
         break;  
      case 24:
         SNI2CSEN_sensorsCount ++;
         LM75A_read_temp(LM75ADDR2);
         SNI2CSEN_state ++;
         break;
      case 25:
         LM75A_2_temperature_last = LM75A_2_temperature;
         LM75A_2_temperature = LM75A_get_Temp();
         (*SNI2CSENAction)(SNI2CSENLM75A_2_temperature);
         SNI2CSEN_state ++;
         break;    
      case 26:// BMP180
         BMP180_device_valid = SNI2CSENcheckDev(BMP180ADDR << 1);
         if (BMP180_device_valid && BMP180_isBMP180()) SNI2CSEN_state ++;
         else {SNI2CSEN_state = 37; BMP180_device_valid = false;};
         break;  
      case 27:
         SNI2CSEN_sensorsCount ++;
         BMP180_calibration0();
         SNI2CSEN_state ++;
         break;
      case 28:
         BMP180_calibration1();
         SNI2CSEN_state ++;
         break;
      case 29:
         BMP180_calibration2();
         SNI2CSEN_state ++;
         break;
      case 30:
         BMP180_calibration3();
         SNI2CSEN_state ++;
         break;
      case 31:
         BMP180_readPressureRAWBegin();
         SNI2CSEN_state ++;
         break;  
      case 32:
         BMP180_readPressureRAWEnd();
         SNI2CSEN_state ++;
         break;      
      case 33:
         BMP180_pressure_last = BMP180_pressure;
         BMP180_pressure = BMP180_get_Pressure();
         (*SNI2CSENAction)(SNI2CSENBMP180_pressure);
         SNI2CSEN_state ++;
         break;         
      case 34:
         BMP180_readTempBegin();
         SNI2CSEN_state ++;
         break;  
      case 35:
         BMP180_readTempEnd();
         SNI2CSEN_state ++;
         break; 
      case 36:
         BMP180_temperature_last = BMP180_temperature;
         BMP180_temperature = BMP180_get_Temp();
         (*SNI2CSENAction)(SNI2CSENBMP180_temperature);
         SNI2CSEN_state ++;
         break; 
      case 37:// AM2320
         AM2320_wake();
         AM2320_device_valid = SNI2CSENcheckDev(AM2320ADDR);
         if (AM2320_device_valid) SNI2CSEN_state ++;
         else SNI2CSEN_state = 43;
         break;
      case 38:
         AM2320_request();
         SNI2CSEN_state ++;
         break;    
      case 39:
         AM2320_read();
         SNI2CSEN_state ++;
         break;  
      case 40:
         AM2320_device_valid = AM2320_crc();
         if (AM2320_device_valid) SNI2CSEN_state ++;
         else SNI2CSEN_state = 43;
         break; 
      case 41:
         SNI2CSEN_sensorsCount ++;
         AM2320_temperature_last = AM2320_temperature;
         AM2320_temperature = AM2320_get_Temp();
         (*SNI2CSENAction)(SNI2CSENAM2320_temperature);
         SNI2CSEN_state ++;
         break; 
      case 42:
         AM2320_humidity_last = AM2320_humidity;
         AM2320_humidity = AM2320_get_Hum();
         (*SNI2CSENAction)(SNI2CSENAM2320_humidity);
         SNI2CSEN_state ++;
         break;
	  case 43:// BH1750
         BH1750_device_valid = SNI2CSENcheckDev(BH1750ADDR);
         if (BH1750_device_valid) SNI2CSEN_state ++;
         else SNI2CSEN_state = 45;
         break; 
      case 44:
         SNI2CSEN_sensorsCount ++;
         BH1750_lightLevel_last = BH1750_lightLevel;
         BH1750_lightLevel = BH1750_getlightLevel();
         (*SNI2CSENAction)(SNI2CSENBH1750_light);
         SNI2CSEN_state ++;
         break; 
      case 45:// PM2005
         PM2005_device_valid = SNI2CSENcheckDev((int8)PM2005ADDR >> 1);
         if (PM2005_device_valid) SNI2CSEN_state ++;
         else SNI2CSEN_state = 48;
         //SNI2CSEN_state ++;
         break;
      case 46:
         //PM2005_init();
         SNI2CSEN_state ++;
         break;
      case 47:
         //PM2005_read();
         SNI2CSEN_state ++;
         break;      
      case 48:// SHT2X
         if (SHT2X_init()) SNI2CSEN_state ++;
         else SNI2CSEN_state = 57;
         break;
      case 49:
         SHT2X_readTemperatureBegin();
         SNI2CSEN_state ++;
         break;
      case 50:
         SNI2CSEN_state ++;
         break;    
      case 51:
         SNI2CSEN_state ++;
         break;  
      case 52:
         SNI2CSEN_state ++;
         break;         
      case 53:
         SHT2X_readTemperatureEnd();
         SHT2X_device_valid = !SNI2CSEN_amountCrcDifferences;
         if (SHT2X_device_valid) {
            SHT2X_temperature_last = SHT2X_temperature;
            SHT2X_temperature = SHT2X_getTemperature();
            SNI2CSEN_sensorsCount ++;
            SNI2CSEN_state ++;
            (*SNI2CSENAction)(SNI2CSENSHT2X_temperature);
         } else SNI2CSEN_state = 57;
         break; 
      case 54:
         SHT2X_readHumidityBegin();
         SNI2CSEN_state ++;
         break; 
      case 55:
         SNI2CSEN_state ++;
         break;                                       
      case 56:
         SHT2X_readHumidityEnd();
         SHT2X_device_valid = !SNI2CSEN_amountCrcDifferences;
         if (SHT2X_device_valid) {
            SHT2X_humidity_last = SHT2X_humidity;
            SHT2X_humidity = SHT2X_getHumidity();
            (*SNI2CSENAction)(SNI2CSENSHT2X_humidity);
         } 
         SNI2CSEN_state ++;
         break; 
#ifndef noSHT3X
      case 57:// SHT3X
         if (SNI2CSENcheckDev(SHT3XADDR0 << 1)) SNI2CSEN_state ++;
         else SNI2CSEN_state = 64;
         break;   
      case 58:
         SHT3X_init(SHT3XADDR0);
         SNI2CSEN_state ++;
         break;
      case 59:
         SHT3X_readBegin(SHT3XADDR0);
         SNI2CSEN_state ++;
         break;
      case 60:
         SNI2CSEN_state ++;
         break;                 
      case 61:
         SHT3X_readEnd(SHT3XADDR0);
         (*SNI2CSENAction)(SNI2CSENSHT3X_0_humidity);
         if (SNI2CSEN_amountCrcDifferences) {
            SNI2CSEN_state = 64;
            SHT3X_0_device_valid = false;
         } else {
            SNI2CSEN_state ++;
            SHT3X_0_device_valid = true;
         }
         break;
      case 62:
         SNI2CSEN_sensorsCount ++;
         SHT3X_0_temperature_last = SHT3X_0_temperature;
         SHT3X_0_temperature = SHT3X_getTemperature();
         (*SNI2CSENAction)(SNI2CSENSHT3X_0_temperature);
         SNI2CSEN_state ++;
         break;
      case 63:
         SHT3X_0_humidity_last = SHT3X_0_humidity;
         SHT3X_0_humidity = SHT3X_getHumidity();
         (*SNI2CSENAction)(SNI2CSENSHT3X_0_humidity);
         SNI2CSEN_state ++;
         break;
      case 64:// SHT3X 1
         if (SNI2CSENcheckDev(SHT3XADDR1 << 1)) SNI2CSEN_state ++;
         else SNI2CSEN_state = 71;
         break;   
      case 65:
         SHT3X_init(SHT3XADDR1);
         SNI2CSEN_state ++;
         break;
      case 66:
         SHT3X_readBegin(SHT3XADDR1);
         SNI2CSEN_state ++;
         break;
      case 67:
         SNI2CSEN_state ++;
         break;                 
      case 68:
         SHT3X_readEnd(SHT3XADDR1);
         (*SNI2CSENAction)(SNI2CSENSHT3X_1_humidity);
         if (SNI2CSEN_amountCrcDifferences) {
            SNI2CSEN_state = 71;
            SHT3X_1_device_valid = false;
         } else {
            SNI2CSEN_state ++;
            SHT3X_1_device_valid = true;
         }
         break;
      case 69:
         SNI2CSEN_sensorsCount ++;
         SHT3X_1_temperature_last = SHT3X_1_temperature;
         SHT3X_1_temperature = SHT3X_getTemperature();
         (*SNI2CSENAction)(SNI2CSENSHT3X_1_temperature);
         SNI2CSEN_state ++;
         break;
      case 70:
         SHT3X_1_humidity_last = SHT3X_1_humidity;
         SHT3X_1_humidity = SHT3X_getHumidity();
         (*SNI2CSENAction)(SNI2CSENSHT3X_1_humidity);
         SNI2CSEN_state ++;
         break;    
#endif     
      case 80: 
         (*SNI2CSENAction)(SNI2CSENdebug);
         SNI2CSEN_state ++;
         break;  
      case 85: 
         SNI2CSEN_state = 0; 
         SNI2CSEN_numberOfSensors = SNI2CSEN_sensorsCount;
         if (SNI2CSEN_sensorsCount == 0) (*SNI2CSENAction)(SNI2CSENnoSensors);
         break;      
      default: SNI2CSEN_state ++; break;
   
   }
}
