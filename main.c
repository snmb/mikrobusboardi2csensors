#include "main.h"
#include "dui.c"
#include "SNI2CSEN.c"

//TASK MESSAGE CHECK
#task(rate = 100 ms, max = 300 us)
void task_SNI2CSENcheck();

void SNI2CSENACT(unsigned int8 param);

//====================================
#task(rate = 20 ms, max = 300 us)
void task_DUIcheck();

void formInit(void);
void formRead(unsigned int8 channel, unsigned int8* data);

//-------------------------------------------------
void task_SNI2CSENcheck() {
   SNI2CSENcheck();
}

void SNI2CSENACT(unsigned int8 param) {
   switch (param) {
      case SNI2CSENBME280_0_humidity:
         DUISendUInt8(0, BME280_0_humidity);
         break;
      case SNI2CSENBME280_0_pressure:
         DUISendUInt32(2, BME280_0_pressure / 133);
         break;
      case SNI2CSENBME280_0_temperature:
         DUISendTemperatureF(1, BME280_0_temperature);
         break;
      case SNI2CSENBME280_1_humidity:
         DUISendUInt8(0, BME280_1_humidity);
         break;
      case SNI2CSENBME280_1_pressure:
         DUISendUInt32(2, BME280_1_pressure / 133);
         break;
      case SNI2CSENBME280_1_temperature:
         DUISendTemperatureF(1, BME280_1_temperature);
         break;
      case SNI2CSENLM75A_0_temperature:
         DUISendTemperatureF(1, LM75A_0_temperature);
         break;  
      case SNI2CSENLM75A_1_temperature:
         DUISendTemperatureF(1, LM75A_1_temperature);
         break;  
      case SNI2CSENAM2320_temperature:
         DUISendTemperatureF(1, AM2320_temperature);
         break;  
      case SNI2CSENAM2320_humidity:
         DUISendUInt8(0, AM2320_humidity);
         break; 
      case SNI2CSENBMP180_pressure:
         DUISendUInt32(2, BMP180_pressure / 133);
         break;
      case SNI2CSENBMP180_temperature:
         DUISendTemperatureF(1, BMP180_temperature);
         break; 
      case SNI2CSENBH1750_light:
         DUISendUInt16(3, BH1750_lightLevel);
         break; 
      case SNI2CSENPM2005_PM25:
         DUISendUInt16(4, PM2005_PM25Level);
         break;  
      case SNI2CSENPM2005_PM10:
         DUISendUInt16(5, PM2005_PM10Level);
         break; 
      case SNI2CSENSHT2X_temperature:
        // DUISendTemperatureF(6, SHT2X_temperature);
         DUISendTemperatureF(1, SHT2X_temperature);
         break; 
      case SNI2CSENSHT2X_humidity:
         DUISendUInt8(0, SHT2X_humidity);
         break;    
      case SNI2CSENSHT3X_0_temperature:
         DUISendTemperatureF(1, SHT3X_0_temperature);
         break;   
      case SNI2CSENSHT3X_0_humidity:
         DUISendUInt8(0, SHT3X_0_humidity);
         break;   
      case SNI2CSENSHT3X_1_temperature:
         DUISendTemperatureF(1, SHT3X_1_temperature);
         break;   
      case SNI2CSENSHT3X_1_humidity:
         DUISendUInt8(0, SHT3X_1_humidity);
         break;                                             
      case SNI2CSENdebug:
         DUISendUInt16(50, SNI2CSEN_debug);
         break;       
     
   }

   DUISendStateTrueFalse(10, BME280_0_device_valid);
   DUISendStateTrueFalse(11, BME280_1_device_valid);
   DUISendStateTrueFalse(12, LM75A_0_device_valid);
   DUISendStateTrueFalse(13, LM75A_1_device_valid);
   DUISendStateTrueFalse(14, BMP180_device_valid);
   DUISendStateTrueFalse(15, AM2320_device_valid);
   DUISendStateTrueFalse(16, BH1750_device_valid);
   DUISendStateTrueFalse(17, SHT2X_device_valid);
   DUISendStateTrueFalse(18, SHT3X_0_device_valid);
   DUISendStateTrueFalse(19, SHT3X_1_device_valid);

   
}

void task_DUIcheck() {
   DUICheck();
}

void formInit(void) {
   

   DUIAddLed(10, 0, (char*)"BME280_0");
   DUIAddLed(11, 0, (char*)"BME280_1");
   DUIAddLed(12, 0, (char*)"LM75_0");   
   DUIAddLed(13, 0, (char*)"LM75_1");     
   DUIAddLed(14, 0, (char*)"BME085"); 
   DUIAddLed(15, 0, (char*)"AM2320");
   DUIAddLed(16, 0, (char*)"BH1750"); 
   DUIAddLed(17, 0, (char*)"SHT2X");   
   DUIAddLed(18, 0, (char*)"SHT3X_0"); 
   DUIAddLed(19, 0, (char*)"SHT3X_1");   


   DUIAddLabel(0, (char*)"Humidity, %");
   DUIAddLabel(1, (char*)"Temperature");
   DUIAddLabel(2, (char*)"Pressure, mmHg");
   DUIAddLabel(3, (char*)"Light, lX");  
   DUIAddLabel(4, (char*)"Dust, ppm");
   DUIAddLabel(50, (char*)"Debug");

}

void formRead(unsigned int8 channel, unsigned int8* data) {

   if (channel == 4) {
   //   Temperature = DUIReadInt16(1, data);
   }

}

//implementation
void initialization() {
    SET_TRIS_A( 0x00 );
    SET_TRIS_B( 0x01 );
    SET_TRIS_C( 0x80 );
    DUIInit();
    DUIOnFormInit = formInit;
    DUIOnReadData = formRead;
    SNI2CSENAction = SNI2CSENACT;
    output_low(PWM);
    output_low(PIN_A1);
    output_low(PIN_A2);
    output_low(PIN_A3);
}

void main() {
   initialization();
   rtos_run();
}
